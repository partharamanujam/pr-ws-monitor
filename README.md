# pr-ws-monitor

Websockets connection monitoring

## Philosophy

Monitor websockets for activity, and send PINGS for in-active sockets. Close un-responsive connections automatically using periodic auditing.

## Installation

```bash
$ npm install pr-ws-monitor
```

## APIs

The module exports a class WsMonitor(wss[, options]) with two methods:
* start
* stop

```js
var WsMonitor = require('pr-ws-monitor');
    ws = require('ws');

var wss, wsmon;

wss = new ws.Server({
    port: 9090,
    clientTracking: true
});

wsmon = new WsMonitor(wss);

wsmon.start(); // start monitoring all connections to wss
wsmon.stop(); // stop monitoring
```

The options are a JSON object:
* ts : time-stamp accuracy (default: 1-sec)
* error : un-responsive duration after which to close the socket (default: 60-sec)
* audit : frequency of auditing connections (default: error/4)

## Usage

Refer to test [spec.js](https://gitlab.com/partharamanujam/pr-ws-monitor/blob/master/test/spec.js) implementation for usage details.

## Test

```bash
$ npm install # inside pr-ws-monitor
$ npm test
```
