"use strict";

var WebSocket = require("ws"),
    WsMonitor = require("../index");

/* globals describe, beforeEach, afterEach, it */
describe("testing ws-monitor - takes a while...", function testExpressWs() {
    var wss, ws, wsmon,
        tracking = false,
        opts = {
            "ts": 1000, // 1-sec accuracy
            "error": 60000, // 1-min after which socket is considered un-responsive
            "audit": 15000 // audit socket connection every 15-sec
        },
        idx = 1,
        duration = {
            "1": 1,
            "2": 15,
            "3": 15,
            "4": 90
        };

    this.timeout(120000); // eslint-disable-line no-invalid-this, no-magic-numbers
    beforeEach(function before() {
        wss = new WebSocket.Server({
            "port": 9090,
            "clientTracking": tracking
        });
        wsmon = new WsMonitor(wss, opts);
        console.log("   test-case duration is approx. (secs): " + // eslint-disable-line no-console
            duration[idx]);
        idx++;
    });
    afterEach(function after(done) {
        wsmon.stop();
        wss.close(done);
    });
    it("Check client-tracking", function checkTracking(done) {
        wsmon.once("error", function onError() {
            tracking = true; // for following tests
            done();
        });
        wsmon.start();
    });
    it("PING existing socket", function pingExisting(done) {
        ws = new WebSocket("http://localhost:9090");
        ws.on("ping", function onPing() {
            ws.close();
            done();
        });
        wsmon.start();
    });
    it("PING new socket", function pingExisting(done) {
        wsmon.start();
        ws = new WebSocket("http://localhost:9090");
        ws.on("ping", function onPing() {
            ws.close();
            done();
        });
    });
    it("CLOSE paused socket", function pingExisting(done) {
        wsmon.start();
        ws = new WebSocket("http://localhost:9090");
        ws.on("close", function onClose() {
            done();
        });
        ws.on("open", function onClose() {
            ws.pause();
            setTimeout(function resume() { // eslint-disable-line max-nested-callbacks
                ws.resume();
            }, 90000); // eslint-disable-line no-magic-numbers
        });
    });
});